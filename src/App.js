// @flow
import React, {Component} from 'react';
import type {GiphyObject} from "./providers/providerTypes";
import {MuiThemeProvider} from "material-ui/styles";
import AppBarComponent from "./components/appBar/AppBarComponent";
import HomePageComponent from "./components/homePage/HomePageComponent";
import store from "./redux/store";
import {Provider} from "react-redux";

type StateTypes = {
    fetching: boolean,
    memes: GiphyObject[],
};

class App extends Component<void, StateTypes> {

    state: StateTypes = {
        fetching: true,
        memes: [],
    };

    render = (): React$Element => (
        <Provider store={store}>
            <MuiThemeProvider>
                <div>
                    <AppBarComponent/>
                    <HomePageComponent/>
                </div>
            </MuiThemeProvider>
        </Provider>
    )
}

export default App;
