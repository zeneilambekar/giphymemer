// @flow
import {getConfig} from "../Config";

const {
    thirdParty: {
        giphy: {
            apiKey,
            host,
            version
        }
    }
} = getConfig();

export const fetchTrendingMemes = async (rating: string = 'G',
                                         limit: number,
                                         offset: number) => {

    const endpoint = `gifs/trending`;
    const params = {limit, offset, rating};

    return await makeGetRequest(endpoint, params);

};

export const fetchMemesBySearchQuery = async (rating: string = 'G',
                                              language: string = 'en',
                                              query: string,
                                              limit: number,
                                              offset: number) => {

    const endpoint = `gifs/search`;
    const params = {rating, lang: language, q: query, limit, offset};

    return await makeGetRequest(endpoint, params);

};

const makeGetRequest = async (endpoint,
                              params): Promise<any> => {

    const paramsWithAuth = {
        ...params,
        api_key: apiKey
    };

    const paramsString = Object.keys(paramsWithAuth).reduce((acc, it) =>
        `${acc}${it}=${paramsWithAuth[it]}&`, "?");

    const url = `https://${host}/${version}/${endpoint}${paramsString}`;

    const response = await fetch(url, {mode: 'cors'});
    const {data} = await response.json();

    return {data};

};
