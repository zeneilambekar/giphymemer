// @flow
import { CookieStorage } from 'cookie-storage';
const cookieStorage = new CookieStorage();

export const storeSearchString = async (searchString: string) : Promise<void> => {
    const strings = JSON.parse(cookieStorage.getItem('search') || "{}", null, '\t');

    const newStrings = {
        ...strings,
        [searchString]: 1,
    };

    cookieStorage.setItem('search', JSON.stringify(newStrings, null, '\t'));

};

export const getAllSearchStrings = async () : Promise<void> => Object.keys(
    JSON.parse(cookieStorage.getItem('search') || "{}")
);
