// @flow
export type GiphyUser = {
    avatar_url: string,
    banner_url: string,
    profile_url: string,
    username: string,
    display_name: string,
    twitter: string,
    is_verified: boolean
};

export type GiphyImages = {
    fixed_width: {
        url: string
        width: string,
        height: string,
        size: string,
    },
}

export type GiphyObject = {
    id: string,
    url: string,
    username: string,
    rating: string,
    import_datetime: string,
    trending_datetime: string,
    title: string,
    images: GiphyImages,
    user: GiphyUser
}

export type GiphyResponse = {
    data: GiphyObject[],
    pagination: {
        total_count: number,
        offset: number,
        count: number
    }
}
