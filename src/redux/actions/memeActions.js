// @flow
import type {
    ActionType,
    MemesFetchedActionType,
    QueryChangedActionType,
    ThunkType
} from "../actionTypes";

import {FETCHING_MORE, MEME_FETCH_STARTED, MEMES_FETCHED, QUERY_CHANGED} from "../actionTypes";
import type {GiphyObject} from "../../providers/providerTypes";
import * as memeService from "../services/memeService";

export const memesFetchStarted = (): ActionType => ({
    type: MEME_FETCH_STARTED
});

export const fetchingMoreStarted = (): ActionType => ({
    type: FETCHING_MORE
});

export const memesFetched = (memes: GiphyObject[],
                             mode: 'trending' | 'search',
                             offset: number): MemesFetchedActionType => ({
    type: MEMES_FETCHED,
    memes,
    mode,
    offset,
});

export const queryChanged = (query: string): QueryChangedActionType => ({
    type: QUERY_CHANGED,
    query,
});

export const fetchTrendingMemes = (): ThunkType => (dispatch, getState) =>
    memeService.fetchTrendingMemes(dispatch, getState);

export const fetchMemesBySearchQuery = (query: string): ThunkType => (dispatch, getState) =>
    memeService.fetchMemesBySearchQuery(query, dispatch, getState);

export const fetchMoreMemes = (): ThunkType => (dispatch, getState) =>
    memeService.fetchMoreMemes(dispatch, getState);


