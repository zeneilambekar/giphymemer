// @flow
import type {StoreSearchStringActionType, ThunkType} from "../actionTypes";
import {SET_SEARCH_STRINGS, STORE_SEARCH_STRING} from "../actionTypes";
import * as memeService from "../services/memeService";

export const storeSearchString = (searchString: string) : StoreSearchStringActionType => ({
    type: STORE_SEARCH_STRING,
    searchString
});

export const setSearchStrings = (searchStrings: string[]) : StoreSearchStringActionType => ({
    type: SET_SEARCH_STRINGS,
    searchStrings,
});

export const fetchSearchStrings = () : ThunkType => (dispatch) =>
    memeService.fetchSearchStrings(dispatch);
