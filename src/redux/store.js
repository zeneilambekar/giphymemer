import thunk from 'redux-thunk';
import {combineReducers, applyMiddleware, createStore} from 'redux';

const reducerModules ={
    'memes': require('./reducers/memeReducer').default,
    'cache': require('./reducers/cacheReducer').default
};

const reducers = combineReducers(reducerModules);
export default createStore(reducers, applyMiddleware(thunk));
