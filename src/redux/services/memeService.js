// @flow
import {fetchingMoreStarted, memesFetched, memesFetchStarted} from "../actions/memeActions";
import * as api from "../../providers/giphyApi";
import * as cacheProvider from "../../providers/cacheProvider";
import {storeSearchString} from "../actions/cacheActions";
import * as memeActions from "../actions/memeActions";
import type {GiphyObject, GiphyResponse} from "../../providers/providerTypes";
import type {ActionType} from "../actionTypes";
import * as cacheActions from "../actions/cacheActions";

export const fetchTrendingMemes = (dispatch: any,
                                   getState: any) => {

    const {memes: {rating}} = getState();

    return encapsulate(dispatch, 'trending', 1, async () => {
        const {data: memes} = await api.fetchTrendingMemes(rating, 27, 0) || {};

        return memes;
    });


};

export const fetchMoreMemes = (dispatch: any,
                               getState: any) => {

    const {memes: {memes: oldMemes, mode, offset, rating, language, query}} = getState();
    const newOffset = offset + 1;

    console.log("Here: " + newOffset);

    const fetcher = mode === 'search' ?
        () => api.fetchMemesBySearchQuery(rating, language, query, 27, newOffset*27) :
        () => api.fetchTrendingMemes(rating, 27, newOffset*27);

    return encapsulate(dispatch, mode, newOffset, async () => {
        const {data: memes} = await fetcher();
        return [...oldMemes, ...memes];

    }, fetchingMoreStarted());

};

export const fetchSearchStrings = async (dispatch: any) => {

    const searchStrings = await cacheProvider.getAllSearchStrings();
    const setSearchStringsAction = cacheActions.setSearchStrings(searchStrings);
    dispatch(setSearchStringsAction);

};

export const fetchMemesBySearchQuery = (query: string,
                                        dispatch: any,
                                        getState: any) => {


    const {memes: {rating, language}} = getState();

    return encapsulate(dispatch, 'search', 1, async () => {

        const queryChangedAction = memeActions.queryChanged(query);
        dispatch(queryChangedAction);

        const {data: memes} = await api.fetchMemesBySearchQuery(rating, language, query, 27, 0) || {};

        await cacheProvider.storeSearchString(query);

        const storeSearchStringAction = storeSearchString(query);
        dispatch(storeSearchStringAction);

        return memes;

    });


};

const encapsulate = async (dispatch: any,
                           returnMode: string,
                           nextOffset: number,
                           runnable: () => Promise<GiphyObject[]>,
                           fetchStartAction: ActionType) => {
    const memesFetchStartedAction = fetchStartAction || memeActions.memesFetchStarted();
    dispatch(memesFetchStartedAction);

    const memes = await runnable();

    const memesFetchedAction = memeActions.memesFetched(memes, returnMode, nextOffset);
    dispatch(memesFetchedAction);
};
