import type {GiphyObject} from "../providers/providerTypes";

export type ActionType = {
    type: string,
};

export type QueryChangedActionType = ActionType & {
    query: string,
}

export type MemesFetchedActionType = ActionType & {
    memes: GiphyObject[],
    mode: string,
    offset: number,
}

export type StoreSearchStringActionType = ActionType & {
    searchString: string
};

export type SetSearchStringsActionType = ActionType & {
    searchStrings: string[],
};

export type ThunkType = (?any, ?any) => Promise<void>

export const MEME_FETCH_STARTED = `MEME_FETCH_STARTED`;

export const MEMES_FETCHED = `MEMES_FETCHED`;

export const STORE_SEARCH_STRING = `STORE_SEARCH_STRING`;

export const FETCHING_MORE = `FETCHING_MORE`;

export const QUERY_CHANGED = `QUERY_CHANGED`;

export const SET_SEARCH_STRINGS = `SET_SEARCH_STRINGS`;
