// @flow
import type {ActionType, StoreSearchStringActionType} from "../actionTypes";
import {SET_SEARCH_STRINGS, STORE_SEARCH_STRING} from "../actionTypes";
import {uniq} from 'ramda';

type StateTypes = {
    strings: string[]
};

const initialState = {
    strings: []
};

export default <T: ActionType>(state: StateTypes = initialState,
                               action: T): StateTypes => {

    switch(action.type) {
        case STORE_SEARCH_STRING:
            const {searchString} = (action: StoreSearchStringActionType);
            return {
                ...state,
                strings: uniq([
                    searchString,
                    ...state.strings,
                ])
            };

        case SET_SEARCH_STRINGS:
            const {searchStrings: strings} = action;
            return {
                ...state,
                strings,
            }
    }

    return state;

};
