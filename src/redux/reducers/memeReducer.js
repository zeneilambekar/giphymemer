// @flow
import type {ActionType, MemesFetchedActionType, QueryChangedActionType} from "../actionTypes";
import type {GiphyObject} from "../../providers/providerTypes";

import { FETCHING_MORE, MEME_FETCH_STARTED, MEMES_FETCHED, QUERY_CHANGED } from "../actionTypes";

type StateTypes = {
    isFetching: boolean,
    isFetchingMore: boolean,
    memes: GiphyObject[],
    mode: 'search' | 'trending',

    rating: string,
    language: string,

    query?: string,

    offset: number

};

const initialState: StateTypes = {
    isFetching: false,
    memes: [],
    mode: 'trending',

    rating: 'G',
    language: 'en',

    offset: 1,

};

export default <T: ActionType>(state: StateTypes = initialState,
                               action: T): StateTypes => {

    switch(action.type) {
        case MEME_FETCH_STARTED:
            return {
                ...state,
                isFetching: true
            };

        case MEMES_FETCHED:
            const {memes, mode, offset} = (action: MemesFetchedActionType);
            return {
                ...state,
                isFetching: false,
                isFetchingMore: false,
                memes,
                mode,
                offset,
            };

        case QUERY_CHANGED:
            const {query} = (action: QueryChangedActionType);
            return {
                ...state,
                query,
            };

        case FETCHING_MORE:
            return {
                ...state,
                isFetchingMore: true
            };

    }

    return state;

};
