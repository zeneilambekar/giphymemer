// @flow
import React, {Component} from 'react';
import type {GiphyObject} from "../../providers/providerTypes";
import {connect} from "react-redux";
import {
    fetchMemesBySearchQuery, fetchMoreMemes,
    fetchTrendingMemes
} from "../../redux/actions/memeActions";
import {
    CircularProgress,
    GridList,
    GridTile,
    Subheader,
    FlatButton
} from "material-ui";
import SearchBar from 'material-ui-search-bar';
import {fetchSearchStrings} from "../../redux/actions/cacheActions";

type PropTypes = {
    isFetching: boolean,
    isFetchingMore: boolean,
    memes: GiphyObject[],
    mode: string,
    searchStrings: string[],
    currentQuery: string,

    onSearch: () => void,
    onFetchTrendingMemes: () => void,
    onLoadSearchStrings: () => void,
    onLoadMore: () => void,
};

type StateTypes = {
    currentSearchString: string,
}

const HomePageComponent = class extends Component<PropTypes, StateTypes> {

    state = {
        currentSearchString: ""
    };

    componentDidMount = () => {
        this.props.onFetchTrendingMemes();
        this.props.onLoadSearchStrings();
    };

    render = (): React$Element => {
        const {isFetching} = this.props;
        const bottomComponent = isFetching ? this.renderLoading() : this.renderGridList();

        return (
            <div>
                {this.renderSearchBar()}
                {bottomComponent}
            </div>
        )

    };

    handleSearchChange = (currentSearchString) => {
        const {mode, onFetchTrendingMemes} = this.props;
        mode === 'search' && currentSearchString.length === 0 && onFetchTrendingMemes();

        this.setState({currentSearchString});

    };

    renderSearchBar = (): React$Element => {
        const {onSearch, mode, searchStrings} = this.props;
        const {currentSearchString} = this.state;
        const searchStringValue = mode === 'search' && searchStrings[0];

        return (
            <SearchBar dataSource={searchStrings}
                       value={searchStringValue || ""}
                       onChange={this.handleSearchChange}
                       onRequestSearch={() => onSearch(currentSearchString)}/>
        )
    };

    renderGridList = (): React$Element => {
        const {currentQuery} = this.props;
        return (
            <div style={styles.root}>
                <GridList cellHeight={180}
                          cols={3}
                          style={styles.gridList}>
                    <Subheader>{
                        this.props.mode === 'search' ?
                            `Results for "${currentQuery}"` :
                            "Trending"
                    }</Subheader>
                    {

                        this.props.memes.map(({id, title, username, images, url}) => (
                            <GridTile key={id}
                                      title={title}
                                      subtitle={<span>by {username}</span>}
                                      style={styles.gridTile}
                                      onClick={() => this.onItemTap(url)}>
                                <img src={images.fixed_width.url}/>
                            </GridTile>
                        ))
                    }
                </GridList>
                {this.renderLoadMore()}
            </div>
        )
    };

    onItemTap = (url: string) : void => window.location.assign(url);

    renderLoadMore = (): React$Element => (
        this.props.isFetchingMore ?
            <CircularProgress thickness={7} size={50}/>
            :
            <FlatButton label="Load More" onClick={() => this.loadMore()}
                        style={styles.flatButton}/>
    );

    renderLoading = (): React$Element => (
        <div style={styles.root}>
            <CircularProgress thickness={7} size={100}/>
        </div>
    );

    loadMore = () => this.props.onLoadMore();

};

const mapStateToProps = ({
                             memes: {
                                 isFetching,
                                 isFetchingMore,
                                 memes,
                                 query: currentQuery,
                                 mode
                             },
                             cache: {
                                 strings: searchStrings
                             }
                         }) => ({

    isFetching,
    isFetchingMore,
    memes,
    mode,
    searchStrings,
    currentQuery,
});

const mapDispatchToProps = (dispatch) => ({
    onSearch: query => {
        const action = fetchMemesBySearchQuery(query);
        dispatch(action);
    },

    onLoadMore: () => {
        const action = fetchMoreMemes();
        dispatch(action);
    },

    onFetchTrendingMemes: () => {
        const action = fetchTrendingMemes();
        dispatch(action);
    },

    onLoadSearchStrings: () => {
        const action = fetchSearchStrings();
        dispatch(action);
    }

});

export default connect(mapStateToProps, mapDispatchToProps)(HomePageComponent);

const styles = {
    root: {
        marginTop: 50,
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
    },
    gridList: {
        width: '100%',
        overflowY: 'auto',
    },
    gridTile: {
        cursor: 'pointer',
    },

    flatButton: {
        marginTop: 20,
        marginBottom: 20
    }

};