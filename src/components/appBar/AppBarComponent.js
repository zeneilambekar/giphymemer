// @flow
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {AppBar, Drawer, FlatButton, List, ListItem} from "material-ui";
import {History, Close} from 'material-ui-icons'
import * as memeActions from "../../redux/actions/memeActions";

type StateTypes = {
    drawerOpen: boolean,

};

type PropTypes = {
    strings: string[],
    onSearchMeme: string => void,
}

class AppBarComponent extends Component<PropTypes, StateTypes> {

    state = {
        drawerOpen: false
    };

    handleItemClick = (string) => {
        this.props.onSearchMeme(string);
        this.setState({drawerOpen: false});
    };

    render = (): React$Element => (
        <div>
            {this.rightDrawer()}
            <AppBar title="Memer Pro"
                    iconElementRight={<History style={styles.iconRight}/>}
                    onRightIconButtonClick={() => this.setState({drawerOpen: true})}/>
        </div>

    );

    leftDrawer = () :React$Element => (
        <div>

        </div>
    );

    rightDrawer = (): React$Element => (
        <Drawer width={200}
                openSecondary={true}
                open={this.state.drawerOpen}
                docked={false}>
            <FlatButton icon={<Close/>}
                        style={styles.closeButton}
                        onClick={() => this.setState({drawerOpen: false})}/>

            <List style={styles.list}>
                {
                    this.props.strings.length > 0 ? this.props.strings.map(string =>
                        <ListItem primaryText={string}
                                  key={string}
                                  onClick={() => this.handleItemClick(string)}/>
                    ) : (
                        <h1 style={styles.noResults}>No Recent Searches</h1>
                    )
                }
            </List>
        </Drawer>
    );



}

const styles = {
    noResults: {
        paddingLeft: 5,
        paddingRight: 5,
        alignSelf: 'center',
    },

    iconRight: {
        color: 'white',
        align: "center",
        marginTop: 10,
        marginRight: 20,
    },

    closeButton: {
        float: 'right',
        marginBottom: 20,
    },

    list: {
        marginTop: 30,
    },

    listItem: {
        color: 'black'
    }
};

const mapStateToProps = ({cache: {strings}}) => ({
    strings
});

const mapDispatchToProps = (dispatch: any) => ({
    onSearchMeme: (term) => {
        const action = memeActions.fetchMemesBySearchQuery(term);
        dispatch(action);
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(AppBarComponent);
