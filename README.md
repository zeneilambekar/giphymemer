# Project Setup
To setup the project you'll require Node 8.9.4 (LTS), yarn or npm and a browser of your choice.
The project was tested on the latest version of Mozilla Firefox.

Make sure you have `create-react-app` installed. To do so, type 
`npm install -g create-react-app`

Once the project is cloned, run `yarn` or `npm install` in the project's
 root directory to install all of the required dependencies.

To run the project, either execute `yarn start` or `npm start` 
inside the project's root directory.

Then navigate to `http://localhost:3000` on your browser.

# Running
The home page should display trending gifs. To load more, navigate to
the bottom part of the page and click *Load More*

To search for specific memes, enter words in the *Search Bar* and hit Enter.

The *Search Bar* stores all of the previous searches performed and a dropdown of
autocomplete terms is displayed once you start typing.
  
There is a right drawer which is displayed on clicking the
history icon on the right corner of the App Bar. This provides quick
access to all search strings previously entered. Clicking on any
of these will cause app to perform the search again.  

# Future Work
The code has the capability of filtering according to rating 
(which currently defaults to **G**) and language 
(which currently defaults to **en**)